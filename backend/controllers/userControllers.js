const User=require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const accessTokenSecret=process.env.TOKEN || "employeeAccessToken";
const saltRounds=10;

//defining the controller function with the logic to login users of the system
const loginUser=async (req,res)=>{
    const { username, password } = req.body;
    //validating the username and password
    const usernameExists = await User.findOne({username: username});
    if(usernameExists)
    {
        const passwordCheck = await bcrypt.compare(password, usernameExists.password);
        if(passwordCheck)
        {
         //generating and returning the token
         const token = jwt.sign({ username: usernameExists.username,  id:usernameExists._id }, accessTokenSecret);   
         res.status(200).header('token', token).send(token);   
        }
        else
        {
            return res.status(404).send("Password is incorrect for the user");
        }
       
    }
    else
    {
        return res.status(404).send('Username does not exist');
    }
    



};

//defining the controller function with the logic register users of the system
const registerUser=async (req,res)=>{
    const { username, password } = req.body;

    //checking if user already exists
    try{
        const userExists = await User.findOne({ username: username});
        if(userExists) 
        {
            return res.status(409).send('This user already exists');
        }

    }
    catch(error){
        res.status(400).send(error); //error checking using try catch
    }

    //hashing the password
    bcrypt.genSalt(saltRounds, function(error, salt) {
        bcrypt.hash(password, salt, function(error, hash) {
        // Store hash in database here
        const user = new User({
            username: username,
            password: hash
        });
        user.save().then((result)=>{
            console.log(result);
             res.status(200).send(result);})
             .catch((err)=>{
                 console.log(err);
             res.status(400).send(err);});
        });
      });

};

//exporting these functions
module.exports={
    registerUser,
    loginUser,
}