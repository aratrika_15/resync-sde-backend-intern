const mongoose=require('mongoose');
const Schema= mongoose.Schema;

//creating the database schema for Employees
const employeeSchema= new Schema({
    e_id:{
        type: String,
        required:true,
        unique: true, 
    },
    firstName:{
        type:String,
        required:true,
    },
    lastName:{
        type:String,
        required:true,
    },
},
    {timestamps:true}
);

//defining the model and then exporting this model for usage in the backend
const Employee= mongoose.model('Employee', employeeSchema);
module.exports=Employee;