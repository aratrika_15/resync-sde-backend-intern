const mongoose=require('mongoose');
const Schema= mongoose.Schema;

//defining locally used variables
let minLengthPass=8;
let maxLengthPass=100;
let minLengthName=5;
let maxLengthName=20;


//defining the schema for Users in the database
const userSchema= new Schema({
username:{
    type:String,
    required: true,
    min: minLengthName,
    max:maxLengthName,
    
},
password:{
    type: String,
    required: true,
    min: minLengthPass,
    max:maxLengthPass
}
}, {timestamps:true});


//defining the model and then exporting this model for usage in the backend
const User= mongoose.model('User', userSchema);
module.exports=User;