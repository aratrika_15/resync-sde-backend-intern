const Employee=require('../models/employee');

//defining the controller function with the logic to get all employees
const getEmployees=async(req,res)=>{
    Employee.find().sort({e_id:1})
    .then((result)=>{
        if(result!=null)
        {let employeeNames=result.map(employee=>{ 
            let name=employee.firstName+' '+employee.lastName;
             return name; });
        res.status(200).send(employeeNames);
        }
        else
        {
            res.status(400).send('There are currently no employees in the database');
        }
    })
    .catch((err)=>{
        res.status(400).send(err);
    });
};

//defining the controller function with the logic to get employee by e_id
const getEmployeeById=async(req,res)=>{
    let employeeId=req.params.e_id;
    console.log(employeeId);
    employeeId=employeeId.toString();
    Employee.findOne({e_id:employeeId})
    .then(result=>{
        console.log(result);
        if(result!=null)
        res.status(200).send(result);
        else
        res.status(404).send("Employee with this e_id does not exist");
    })
    .catch(err =>{

        console.log(err);
        res.status(404).send(err);
    });
};

//function to add employees into mongodb database
const registerEmployee= async(req,res)=>{
    const e_id=req.body.e_id;
    const firstName=req.body.firstName;
    const lastName=req.body.lastName;

    //checking if employee already exists
    try{
        const empExists = await Employee.findOne({ e_id: e_id});
        if(empExists) 
        {
            return res.status(409).send('This employee already exists');
        }

    }
    catch(error){
        res.status(400).send(error); //error checking using try catch
    }

   const employee = new Employee({
       e_id:e_id,
       firstName: firstName,
       lastName: lastName,
   })
   employee.save().then((result)=>{
       console.log(result);
        res.status(200).send(result);})
        .catch((err)=>{
            console.log(err);
        res.status(400).send(err);});

};





//exporting these functions
module.exports={
    getEmployeeById,
    getEmployees,
    registerEmployee
}