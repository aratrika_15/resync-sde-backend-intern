# Restful API for employee management

## Summary
This is a back-end app meant for employee management. It is implemented in Node.js, using the Express framework. It allows the following tasks:
- Register (this allows to register users to the system) 
- Login (this allows users to login to the system)
- Register employee (This allows a user to register an employee onto the database)
- List of names of ALL employees (View All employee names)
- Details of a particular employee (Provides all data of an employee, given the e_id)

It uses MongoDB Atlas to store 2 collections: 
- users
- employees

Note: 
- The current data inside MongoDB Atlas is stored as json files inside the database folder
- A user that can be used to login has username = "Adi" and password = "hello@". This user is already present in MongoDB database.
- An e_id that can be used to retrieve employee details is 1. This employee is already present in MongoDB database.


## Setup
1. Clone the Repo by copying the link and running the git clone command
2. Run npm install, to install all dependencies
3. Navigate to the backend folder
4. Run this command to start server: nodemon index 

Note: It runs on localhost port 5000.


## API Details
1. /register - This is a POST request that allows a new user to register into the system, by passing username and password as a json object in the request body

2. /login - This is a POST request that allows a user to login by passing username and password as a json object in the request body. It returns an authentication token. 

3. /employee/register - This is a POST request to add an employee into the database. The e_id, firstName and lastName must be passed as a json object inside the request body. 

4. /getEmployees - This is a GET request that lists the names of ALL employees in the database

5. /getEmployee/:e_id - This is a GET request that provides all the information about a particular employee given the e_id 

Notes:
- After a login request provides an authentication token, the authentication token is provided in the header of all future requests and it is validated before returning responses
- For any other routes which do not exist, a 404 error message and status code is sent as a response.


## API Testing
This API can be tested using Postman or other apps. Here is a guide of steps that can be followed:
1. Add a user by making a POST request of /register, add a json object in the request body containing username and password. For example- { "username":"belle","password":"hullo"}

2. Login to the system as a user by making a POST request to /login. add a json object in the request body containing username and password. You can use the user that you just registered. You can also use these credentials, since they already exist in the MongoDB database: { "username":"Adi","password":"hello@"}

3. Get a list of employee names by making a GET request to /getEmployees. Provide the authentication token returned in Step 2 inside the Header of this request. In Postman, this can be done by selecting "Authorization", choosing Type "Bearer Token" and then pasting the token inside the box provided.

4. Get all the details of an employee by making a GET request to /getEmployee/:e_id. Provide the employee's id in the e_id parameter. For example, a route would be /getEmployee/1. Provide the authentication token returned in Step 2 inside the Header of this request. In Postman, this can be done by selecting "Authorization", choosing Type "Bearer Token" and then pasting the token inside the box provided. 


## Packages 
- express: framework used to create restful API
- mongoose: used to connect to the mongodb database
- bcrypt- used for hashing passwords that will be stored in the database
- body-parser: used to parse incoming request urls
- jsonwebtoken: used for token creation during login and authentication during future requests
- dotenv: loads environment variables from a .env file into process.env
- nodemon: automatically restarts the node application when file changes in the directory are detected


