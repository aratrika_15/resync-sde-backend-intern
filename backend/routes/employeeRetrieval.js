const express = require('express');

//importing controllers
const empControllers=require('../controllers/empControllers');
//const userControllers=require('../controllers/userControllers');
const auth=require('../controllers/authentication');

const router=express.Router();

//routes for employee related information
router.get('/getEmployees', auth, empControllers.getEmployees);
router.get('/getEmployee/:e_id',auth, empControllers.getEmployeeById);
router.post('/employee/register', auth, empControllers.registerEmployee);
module.exports=router;