//importing the required packages
const bodyParser=require('body-parser'); //parses urls and incoming request bodies
const mongoose=require('mongoose'); //to connect with the mongodb databaxe
const express=require('express');//used to create API
const dotenv = require('dotenv');
dotenv.config();

//importing the routes
const empRoutes=require('./routes/employeeRetrieval');
const userRoutes=require('./routes/userAccess');

//express app
const app=express();

//setting the port and connection url
const dbURL= process.env.DBURL|| "mongodb+srv://EmpDbUser:abcde@cluster0.ou8vv.mongodb.net/EmployeeAPI?retryWrites=true&w=majority";
const port= process.env.PORT || 5000;

//connecting to mongodb database and listening for requests
mongoose.connect(dbURL,{ useNewUrlParser:true, useUnifiedTopology:true})
.then(()=> {app.listen(port); console.log('listening at port '+port);})
.catch((error)=>console.log(error));

//identifies incoming request as a json object
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));


//setting up the routes
app.use('/',userRoutes);
app.use('/',empRoutes);


//handling 404
app.use((req,res)=>{
    console.log("User requested a resource which is unavailable");
    res.status(404).send('Resource not found');
});
