const express = require('express');

//importing controllers
const empControllers=require('../controllers/empControllers');
const userControllers=require('../controllers/userControllers');

//express router
const router=express.Router();

router.post('/login',  userControllers.loginUser);
router.post('/register',userControllers.registerUser);

module.exports=router;